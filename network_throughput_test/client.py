#! /usr/bin/env python3


import logging
import time
import numpy as np
from argparse import ArgumentParser
from opcua import Client
from lib.config import name_space, setup_logging_framework


def get_settings_from_command_arguments(args):
    """
    Get the settings from the the command arguments
    :param args: command line arguments
    :return: the command line parameter values
    """
    argument_parser = setup_command_argument_parser()
    settings = argument_parser.parse_args(args)
    return settings


def setup_command_argument_parser():
    """
    Setup the command arguments parser
    :return: ArgumentParser with the options
    """
    parser = ArgumentParser(description="Client for an OPC-UA network throughput test.  The test consists of two parts:  calling a function in an OPC-UA server to retrieve a bigger amount of bytes and retrieving a smaller amount of bytes through a monitor point.  The test will average the execution time over the runs and print out the min, max and mean time it took for each of the tests.  Start it like this:  \"python3 client.py --ip_address 127.0.0.1 --port 55557 --runs 100\" and it will start an OPC UA client that tries to connect to the OPC-UA server on IP address 127.0.0.1 on the private port 55557 and perform the tests 100 times.")

    parser.add_argument("--ip_address", "-i", dest="host", help="The OPC UA server's IP address or hostname.  Example:  \"127.0.0.1\"", default="127.0.0.1", required=True)

    parser.add_argument("--port", "-p", dest="port", help="The OPC UA server's network port.  Example:  \"55557\"", type = int, default=55557, required=True)

    parser.add_argument("--runs", "-r", dest="runs", help="The number of runs for both OPC-UA network throughput tests.  Example:  \"1000000\"", type = int, default=10000, required=False)

    parser.add_argument("--verbose", "-v", dest="verbose", help="Switch on verbose logging.", action="store_true", default=False, required=False)
    return parser


def exec_function_test(device, function, runs):
    run_times = []
    result = bytes()
    len_result = 0
    for i in range(0, runs):
        # OPC-UA function call
        old_result = result
        t0 = time.clock_gettime(time.CLOCK_REALTIME)
        result = device.call_method(function)
        t1 = time.clock_gettime(time.CLOCK_REALTIME)
        dt = t1 - t0
        if old_result == result:
            logging.error("Run #{} of {}:  No update in OPC-UA function call!".format(i, runs))
        else:
            len_result = len(result)
            run_times.append(dt)
    t = np.array(run_times)
    min = t.min()
    max = t.max()
    mean = t.mean()
    std = t.std()
    throughput = (len_result / mean) / (1024 * 1024)
    logging.error("\nOPC-UA function call:\n\tRuns = {}\n\tpay load = {}[byte]\n\tmin run time = {}[ms]\n\tmax run time = {}[ms]\n\tmean run time = {}[ms]\n\tstddev run time = {}[ms]\n\tmean throughput = {}[Mbyte/s]".format(runs, len_result, min * 1e3, max * 1e3, mean * 1e3, std * 1e3, throughput))

def exec_mp_test(mp, runs):
    run_times = []
    result = bytes()
    len_result = 0
    for i in range(0, runs):
        # OPC-UA monitor point
        old_result = result
        t0 = time.clock_gettime(time.CLOCK_REALTIME)
        result = mp.get_value()
        t1 = time.clock_gettime(time.CLOCK_REALTIME)
        dt = t1 - t0
#        if old_result == result:
#            logging.error("Run #{} of {}:  No update in OPC-UA monitor point value !".format(i, runs))
#            break
#        else:
        len_result = len(result)
        run_times.append(dt)
    t = np.array(run_times)
    min = t.min()
    max = t.max()
    mean = t.mean()
    std = t.std()
    throughput = (len_result / mean) / (1024 * 1024)
    logging.error("\nOPC-UA mp.get_value():\n\tRuns = {}\n\tpay load = {}[byte]\n\tmin run time = {}[ms]\n\tmax run time = {}[ms]\n\tmean run time = {}[ms]\n\tstddev run time = {}[ms]\n\tmean throughput = {}[Mbyte/s]".format(runs, len_result, min * 1e3, max * 1e3, mean * 1e3, std * 1e3, throughput))


def client(args):
    """
    Main function
    :param args: command line arguments
    """
    settings = get_settings_from_command_arguments(args)

    log_level = logging.ERROR
    if settings.verbose is True:
        log_level = logging.INFO
    setup_logging_framework(log_level)

    opcua_client = Client("opc.tcp://{}:{}/".format(settings.host, settings.port))
    opcua_client.connect()
    name_space_index = opcua_client.get_namespace_index(name_space)

    objects = opcua_client.get_objects_node()
    device_name = "Receiver"

    device = objects.get_child("{}:{}".format(name_space_index, device_name))
    exec_function_test(device, "{}:{}".format(name_space_index, "get_all_packets"), settings.runs)
    device = objects.get_child("{}:{}".format(name_space_index, device_name))
    exec_function_test(device, "{}:{}".format(name_space_index, "get_latest_packet"), settings.runs)

    mp_name = "all_packets"
    mp = device.get_child("{}:{}".format(name_space_index, mp_name))
    exec_mp_test(mp, settings.runs)
    mp_name = "latest_packet"
    mp = device.get_child("{}:{}".format(name_space_index, mp_name))
    exec_mp_test(mp, settings.runs)

    opcua_client.close_session()
    opcua_client.close_secure_channel()


if __name__ == '__main__':
    from sys import argv
    client(argv[1:])
