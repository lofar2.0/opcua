import logging
import numpy as np
from array import array
from multiprocessing import Array
from opcua import Server, ua, uamethod
from datetime import datetime
from lib.config import name_space, setup_logging_framework


# Name of the shared memory that is used to store the network packets
all_packets = None
latest_packet = None

name_space_index = None
# Store the monitor_points in this dict
monitor_points = {}

@uamethod
def get_all_packets(parent):
    """
    Return all received packets to the OPC-UA client.
    :returns: An OPC-UA ByteString that contains all received packets.
    """
    global all_packets
    return all_packets.tobytes()

@uamethod
def get_latest_packet(parent):
    """
    Return the latest received packet to the OPC-UA client.
    :returns: An OPC-UA ByteString that contains a single received packets.
    """
    global latest_packet
    return latest_packet.tobytes()

def setup_opc_ua_server(settings, all_packets_array: Array, latest_packet_array: Array):
    global all_packets, latest_packet, monitor_points, name_space_index

    setup_logging_framework()
    all_packets = np.frombuffer(all_packets_array.get_obj(), dtype = np.uint8)
    latest_packet = np.frombuffer(latest_packet_array.get_obj(), dtype = np.uint8)

    # setup our server
    server = Server()

    server.set_endpoint("opc.tcp://" + settings.host + ":" + str(settings.port) + "/LOFAR2.0/Network_test/")
    server.set_server_name("LOFAR2.0 OPC-UA network throughput test server")
    server.set_security_policy([ua.SecurityPolicyType.NoSecurity,])
    # setup our own namespace, not really necessary but should as spec
    name_space_index = server.register_namespace(name_space)

    # Get Objects node.
    objects = server.get_objects_node()

    #Add variables and methods to a device in Objects.
    device_name = "Receiver"
    device = objects.add_folder(name_space_index, device_name)
    method = device.add_method(name_space_index, "get_all_packets", get_all_packets, [], [all_packets.tobytes()])
    method = device.add_method(name_space_index, "get_latest_packet", get_latest_packet, [], [latest_packet.tobytes()])

    # My bookkeeping dict
    monitor_points[device_name]={}

    mp_name = "all_packets"
    var = device.add_variable(name_space_index, mp_name, all_packets.tobytes())
    # Set to be read-only
    var.set_read_only()
    #var.set_writable()
    # Enable value history.
    # Keep values not older than "period" and
    # keep a maximum of "count" values.
    #server.historize_node_data_change(var, period = datetime.timedelta(hours = 1), count = 10)
    # Add the variable to my bookkeeping dict.
    monitor_points[device_name][mp_name] = {"device": device, "variable": var}

    mp_name = "latest_packet"
    var = device.add_variable(name_space_index, mp_name, latest_packet.tobytes())
    var.set_read_only()
    monitor_points[device_name][mp_name] = {"device": device, "variable": var}

    return server

def stop_opc_ua_server():
    global all_packets, latest_packet
    if all_packets is not None:
        del all_packets
    if latest_packet is not None:
        del latest_packet
