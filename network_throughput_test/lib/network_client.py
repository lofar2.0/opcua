#! /usr/bin/env python3


import logging
import socket
from datetime import datetime
from multiprocessing import Value, Array
from lib.config import network_packet_size, setup_logging_framework


def network_client(host: str, port: int, all_packets: Array, latest_packet: Array, stop_execution: Value, verbose = True, simulation = False):
    """
    Network client that receives the network packets from host:port.
    :returns:
    """
    logging_level = logging.INFO
    if verbose is True:
        logging_level = logging.DEBUG
    setup_logging_framework(logging_level = logging_level)

    if simulation is False:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            sock.create_connection((host, port))
        except:
            logging.exception(e)
            return
    else:
        from time import sleep
        from os import urandom

    max_packet_index = len(all_packets) // network_packet_size - 1
    logging.debug("max_packet_index = %d" % (max_packet_index))
    packet_index = 0

    try:
        while stop_execution.value == 0:
            if simulation is False:
                sock.recvmsg_into(latest_packet[0:network_packet_size])
            else:
                latest_packet[0:network_packet_size] = bytes(urandom(network_packet_size))
                sleep(0.0001)

            start_index = packet_index * network_packet_size
            end_index = (packet_index + 1) * network_packet_size
            logging.debug("Packet index = %d, start index = %d, end index = %d, diff = %d" % (packet_index, start_index, end_index, end_index - start_index))
            all_packets[start_index:end_index] = latest_packet[0:network_packet_size]
            logging.debug("Received a packet:  packet_index = %d, last data = %02x...%02x" % (packet_index, latest_packet[0], latest_packet[network_packet_size - 1]))
            if packet_index < max_packet_index:
                packet_index += 1
            else:
                packet_index = 0
    except Exception as e:
        logging.exception(e)
    except KeyboardInterrupt:
        pass
    finally:
        if simulation is False:
            sock.close(sock.fileno())
            logging.debug("Network client socket closed.")

if __name__ == "__main__":
    from sys import argv
    simulation = True
    verbose = True
    host = "127.0.0.1"
    port = 60000
    all_packets = Array('B', 100 * network_packet_size)
    latest_packet = Array('B', network_packet_size)
    network_client(host = host, port = port, all_packets = all_packets, latest_packet = latest_packet, stop_execution = Value('b', 0), verbose = verbose, simulation = simulation)
    shm.unlink()
