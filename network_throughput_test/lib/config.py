import logging


name_space = "http://lofar.eu"
network_packet_size = 1500

def setup_logging_framework(logging_level = logging.ERROR):
    """
    Setup the logging style for a desired logging_level
    :param logging_level:  messages with a logging level lower than this will be filtered out
    """
    logging.basicConfig(format = "%(asctime)s - %(processName)s - %(levelname)s: %(message)s", level = logging_level)
