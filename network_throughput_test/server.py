#! /usr/bin/env python3


import logging
from argparse import ArgumentParser
from multiprocessing import Process, Value, Array
from lib.config import network_packet_size, setup_logging_framework
from lib.network_client import network_client
from lib.opc_interface import setup_opc_ua_server, stop_opc_ua_server


def get_settings_from_command_arguments(args):
    """
    Get the settings from the the command arguments
    :param args: command line arguments
    :return: the command line parameter values
    """
    argument_parser = setup_command_argument_parser()
    settings = argument_parser.parse_args(args)
    return settings


def setup_command_argument_parser():
    """
    Setup the command arguments parser
    :return: ArgumentParser with the options
    """
    parser = ArgumentParser(description="Server for OPC-UA network throughput test.  Start it like this:  \"python3 server.py --ip_address 127.0.0.1 --port 55557 --ip_address_data_host 127.0.0.1 --port_data_host 61616\" and it will start an OPC UA server on IP address 127.0.0.1 on the private port 55557.  It will receive the network data from the host at IP address 127.0.0.1 on port 61616.")

    parser.add_argument("--ip_address", "-i", dest="host", help="The OPC UA server's IP address or hostname.  Example:  \"127.0.0.1\"", default="127.0.0.1", required=True)

    parser.add_argument("--port", "-p", dest="port", help="The OPC UA server's network port.  Example:  \"55557\"", default="55557", required=True)

    parser.add_argument("--ip_address_data_host", "-j", dest="ip_data_host", help="The IP address of the data host.  Example:  \"127.0.0.1\"", default="127.0.0.1", required=True)

    parser.add_argument("--port_data_host", "-q", dest="port_data_host", help="The network port of the data host.  Example:  \"61616\"", default="61616", required=True)

    parser.add_argument("--packet_buffer_size", "-b", dest="size_of_shared_memory", help="The size in bytes of the circular buffer that is used to store the received network packets.  Example:  150000", default="150000", required=False)

    parser.add_argument("--simulation", "-s", dest="simulation", help="Switch on dynamic simulation.  Incoming data packets will be simulated by filling them with random value bytes.", action="store_true")

    parser.add_argument("--verbose", "-v", dest="verbose", help="Switch on verbose logging.", action="store_true", default=False, required=False)
    return parser


def apply_global_settings(settings):
    """
    Apply the settings to the global variables (e.g. the logging level)
    :param settings: command line argument settings
    :return:
    """

def server(args):
    """
    Main function
    :param args: command line arguments
    """
    settings = get_settings_from_command_arguments(args)
    apply_global_settings(settings)

    log_level = logging.ERROR
    if settings.verbose is True:
        log_level = logging.INFO
    setup_logging_framework(log_level)

    all_packets = None
    latest_packet = None
    try:
        all_packets = Array('B', int(settings.size_of_shared_memory))
    except:
        logging.error("Cannot create shared memory of size %d bytes.  Exiting." % (settings.size_of_shared_memory))
        return
    try:
        latest_packet = Array('B', network_packet_size)
    except:
        logging.error("Cannot create shared memory of size %d bytes.  Exiting." % (network_packet_size))
        return

    stop_execution = Value('B', 0)
    p = Process(target = network_client, args = (settings.host, settings.port, all_packets, latest_packet, stop_execution, settings.verbose, settings.simulation))
    p.start()

    from time import sleep

    try:
        opc_ua_server = setup_opc_ua_server(settings, all_packets, latest_packet)
        if opc_ua_server is False:
            raise Exception("Cannot start OPC-UA server.")
        # Start the OPC-UA server.
        opc_ua_server.start()
        while True:
            logging.debug("OPC-UA network throughput test is running...")
            sleep(1.0)
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logging.exception(e)
    finally:
        #close connection, remove subcsriptions, etc
        opc_ua_server.stop()
        stop_opc_ua_server()
        stop_execution.value = 1
        p.join()


if __name__ == "__main__":
    from sys import argv
    server(argv[1:])
