# opcua

This is an OPC UA playground for LOFAR 2.0.

If you use (part of) this software please attribute the use to ASTRON as indicated in the
file NOTICE in the root of the source code repository.