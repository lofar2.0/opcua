#! /usr/bin/env python3

import math
import random
import time
import datetime

from opcua import ua, uamethod, Server


# Change values every sleepTime seconds.
sleepTime = 2
# Store the variables and methods in these dicts
variables = {}
methods = {}
# Disable (True) or enable (False) modifying RW_sine
modify_RW_sine = True


@uamethod
def set_RW_sine(nodeId = None, value = 0.0):
    global modify_RW_sine
    global variables
    global sleepTime

    modify_RW_sine = False
    variables["RW_sine"]["variable"].set_value(value)
    time.sleep(2.0 * sleepTime)
    modify_RW_sine = True
    return modify_RW_sine


if __name__ == "__main__":
    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://dop407:4840/LOFAR2/CS000")
    server.set_server_name("LOFAR2 OPC UA station server")
    # setup our own namespace, not really necessary but should as spec
    uri = "http://lofar.eu"
    idx = server.register_namespace(uri)

    # Get Objects node.
    objects = server.get_objects_node()
    # Now add some folders.
    folder = objects.add_folder(idx, "Uniboard #1")
    subfolder = folder.add_folder(idx, "FPGA #1")
    # Add variables and methods to the subfolder.
    obj = subfolder.add_object(idx, "Parent_object_for_RW_sine_variable")
    var = obj.add_variable(idx, "RW_sine", 0.0)

    # Set RW_time to be writable by clients
    var.set_writable()

    # Enable value history.
    # Keep values not older than "period" and
    # keep a maximum of "count" values.
    #server.historize_node_data_change(var, period = datetime.timedelta(hours = 1), count = 10)

    # Add the variable to my bookkeeping dict.
    variables["RW_sine"] = {"object": obj, "variable": var}

    obj = subfolder.add_object(idx, "Parent_object_for_RO_cosine_variable")
    var = obj.add_variable(idx, "RO_cosine", 0.0)

    # Set RO_time to be read-only
    var.set_read_only()

    # Enable value history.
    # Keep values not older than "period" and
    # keep a maximum of "count" values.
    #server.historize_node_data_change(var, period = datetime.timedelta(hours = 1), count = 10)

    # Add the variable to my bookkeeping dict.
    variables["RO_cosine"] = {"object": obj, "variable": var}


    # Create a method that can modify "RW_sine".
    # args are
    #  nodeid, browsename, method_to_be_called, [input argument types], [output argument types]
    # or
    #  idx, name, method_to_be_called, [input argument types], [output argument types]
    #
    # If [input|output argument types] is specified, child nodes advertising
    # what arguments the method uses and returns will be created.
    #
    # A callback is a method accepting the nodeid of the parent as first
    # argument and variants after that.  It returns a list of variants.
    #
    obj = subfolder.add_object_type(idx, "Parent_object_for_set_RW_sine")
    method = obj.add_method(idx, "set_RW_sine", set_RW_sine, 
        [ua.VariantType.Double], [ua.VariantType.Boolean])
    # Add the method to my bookkeeping dict.
    methods["set_RW_sine"] = {"object": obj, "function": set_RW_sine}

    # Try to import some stuff from an XML file.
    #server.import_xml("LOFAR2_CS000_nodes.xml")

    # Create a "Value out of range" event, a.k.a. alarm
    

    # Start the server.
    server.start()
    value = 0.0
    increment = random.TWOPI / 10.0 * sleepTime

    try:
        while True:
            # Sleep for sleepTime seconds.
            time.sleep(sleepTime)

            # Now continue with the sine/cosine but add a random noise.
            #
            # Keep a copy of the counter in order to add different random values
            # to the variables.
            value += increment

            value_copy = value
            value_copy += (random.random() - 0.5)
            value_copy %= random.TWOPI
            variables["RO_cosine"]["variable"].set_value(math.cos(value_copy))
#             if abs(value_copy) > 0.5:
#                 event.message = "Warning!  The value of abs(RO_cosine) > 0.5."
#                 event.source_node = variables["RO_cosine"]["variable"].get_id()
#                 event.time = datetime.datetime.now()
#                 server.trigger_event(event)

            if modify_RW_sine is True:
                value_copy = value
                value_copy += (random.random() - 0.5)
                value_copy %= random.TWOPI
                variables["RW_sine"]["variable"].set_value(math.sin(value_copy))
#                 if abs(value_copy) > 0.5:
#                     event.message = "Warning!  The value of abs(RW_sine) > 0.5."
#                     event.source_node = variables["RW_sine"]["variable"].get_id()
#                     event.time = datetime.datetime.now()
#                     server.trigger_event(event)
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()

